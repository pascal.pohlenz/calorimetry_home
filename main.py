from functions import m_json
from functions import m_pck

# Änderung von path und des "string-Zusatz" der m_json.archiv_json je nach aktueller Ausarbeitung
path = "datasheets/setup_heat_capacity.json"
folder_path = "datasheets"
metadata = m_json.get_metadata_from_setup(path)
m_json.add_temperature_sensor_serials(folder_path, metadata)
data = m_pck.get_meas_data_calorimetry(metadata)

json_folder = folder_path
data_folder = "data"

m_pck.logging_calorimetry(data, metadata, data_folder, json_folder)
m_json.archiv_json(folder_path, path, data_folder+ '/heat_capacity')

